import express from "express";
import Hotel from "../models/Hotel.js";
import {
   // countByCity,
   // countByType,
   createHotel,
   deleteHotel,
   getHotel,
   
   getAllHotels,
   updateHotel,
 } from "../controllers/hotel.js";
 import { verifyAdmin, verifyToken, verifyUser } from "../utils/verifyToken.js";
// import { createError } from "../utils/error.js";

 const router = express.Router();

 //[Section] CREATE
    router.post("/", verifyAdmin, createHotel);
    
//[Section] UPDATE
    router.put("/:id", verifyUser, updateHotel);

 //[Section] GET
    router.get("/:id", verifyUser, getHotel);

 //[Section] GET All
    router.get("/",verifyAdmin, getAllHotels);

 //[Section] DELETE
    router.delete("/:id",verifyAdmin, deleteHotel);

 export default router