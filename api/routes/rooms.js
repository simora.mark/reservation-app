import express from "express";
import Room from "../models/Room.js";
import {
   createRoom,
   deleteRoom,
   getRoom,
   getAllRooms,
   updateRoom,
 } from "../controllers/room.js";
 import { verifyAdmin, verifyToken, verifyUser } from "../utils/verifyToken.js";


 const router = express.Router();

 //[Section] CREATE
    router.post("/:hotelid", verifyAdmin, createRoom);
    
//[Section] UPDATE
    router.put("/:id", verifyUser, updateRoom);

 //[Section] GET
    router.get("/:id", verifyUser, getRoom);

 //[Section] GET All
    router.get("/",verifyAdmin, getAllRooms);

 //[Section] DELETE
    router.delete("/:id",verifyAdmin, deleteRoom);

 export default router