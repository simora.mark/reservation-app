
import Hotel from "../models/Hotel.js";

//[Section] Create 
    export const createHotel = async (req, res, next) =>{
        const newHotel = new Hotel(req.body);

            try{
                const savedHotel = await newHotel.save();
                res.status(200).json(savedHotel);
            }catch(err){
                next(err);
            }
    }

//[Section]Update

    export const updateHotel = async (req, res, next) =>{
        try{

            const updateHotel = await Hotel.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true });
            res.status(200).json(updateHotel);

        }catch(err){

            next(err);
        }
    }

//[Section]Get

    export const getHotel = async (req, res, next) =>{
        try{

            const getHotel = await Hotel.findById(req.params.id);
            res.status(200).json(getHotel);

        }catch(err){

            next(err);
        }
    }

//[Section]Get all

    export const getAllHotels = async (req, res, next) =>{
        try{

            const getAllHotels = await Hotel.find();
            res.status(200).json(getAllHotels);

        }catch(err){

            next(err);
        }
    }

//[Section]Delete

    export const deleteHotel = async (req, res, next) => {
        try {
        await Hotel.findByIdAndDelete(req.params.id);
        res.status(200).json("Hotel has been deleted.");
        } catch (err) {
        next(err);
        }
    };