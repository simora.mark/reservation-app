import Room from "../models/Room.js";
import Hotel from "../models/Hotel.js";

import { createError } from "../utils/error.js";

//[Section] Create 

export const createRoom = async (req, res, next) => {

    const hotelId = req.params.hotelid;
    const newRoom = new Room(req.body)

    try{
        const savedRoom = await newRoom.save()
        try{
            await Hotel.findByIdAndUpdate(hotelId, {
                $push: { rooms: savedRoom._id },
            });

        } catch (err) {
            next(err);
        }

        res.status(200).json(savedRoom);
    }catch (err) {
        next(err);
    }
}

//[Section]Update

    export const updateRoom = async (req, res, next) =>{
        try{

            const updateRoom = await Hotel.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true });
            res.status(200).json(updateHotel);

        }catch(err){

            next(err);
        }
    }

//[Section]Get

    export const getRoom = async (req, res, next) =>{
        try{

            const getRoom = await Room.findById(req.params.id);
            res.status(200).json(getHotel);

        }catch(err){

            next(err);
        }
    }

//[Section]Get all

    export const getAllRooms = async (req, res, next) =>{
        try{

            const getAllRooms = await Room.find();
            res.status(200).json(getAllRooms);

        }catch(err){

            next(err);
        }
    }

//[Section]Delete

    export const deleteRoom = async (req, res, next) => {
        try {
        await Room.findByIdAndDelete(req.params.id);
        res.status(200).json("Room has been deleted.");
        } catch (err) {
        next(err);
        }
    };