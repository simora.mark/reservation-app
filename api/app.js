// to use import need to add type: module in package.json for es6 modules
import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";

import authRoute from "./routes/auth.js"
import hotelsRoute from "./routes/hotels.js"
import usersRoute from "./routes/users.js"
import roomsRoute from "./routes/rooms.js"
// import roomsRoute from "./routes/rooms.js"
// import usersRoute from "./routes/users.js"

dotenv.config();

//[Section] Server Setup
    const app = express();
    const secret = process.env.CONNECTION_STRING;
    const port = process.env.PORT;

//[SECTION] Database Connection
//Initial connection to mongoDB

const connect = async () => {
    try{
        await mongoose.connect(secret)
        console.log("Connected to mongoDB")
    }catch (error){
        throw(error);
    }
}; 
// if disconnected reattempt to connect
    mongoose.connection.on('disconnected', () => {console.log("MongoDB is Disconnected!")});


 //[Section] Application Routes //middlewares
    app.use(cookieParser())
    app.use(express.json());
    app.use(cors({origin:"*"}));
    app.use('/api/auth', authRoute);
    // app.use('/api/users', usersRoute);
    app.use('/api/users', usersRoute);
    // app.use('/api/hotels', hotelsRoute);
    app.use('/api/hotels', hotelsRoute);
    app.use('/api/rooms', roomsRoute);
    // app.use('/api/rooms', roomsRoute);

    // Error Middlewear
    app.use((err, req, res, next) => {
        const errorStatus = err.status || 500
        const errorMessage = err.message || "Something went wrong!"

        return res.status(errorStatus).json({
            success: false,
            status: errorStatus,
            message: errorMessage,
            stack: err.stack,
        });
    });
    


//[SECTION] Gateway Response

    app.listen(port, () => {
        connect()
        console.log(`Server is running on port ${port}`)
    });